# Wiki - Built with Python and Flask

A wiki server that uses text files on the local filesystem to store page data.

## Screenshots

![A screenshot of the wiki homepage. Screenshot includes a page title and links.](Screenshots/Homepage.png)

![A screenshot of the wiki view page. Screenshot includes content of game and edit/history links.](Screenshots/Viewpage.png)

![A screenshot of the wiki edit page. Screenshot includes a form to edit the content of the view page.](Screenshots/Editpage.png)

![A screenshot of the wiki history page. Screenshot includes data of description of edit, editor's name, email, and time of edit.](Screenshots/Historypage.png)

## Installations

This project has been tested with Python 3.7.3. To install the necessary dependencies, first create and activate a virtual environment.

```
# Create a directory to store virtual environments.
mkdir "$HOME/venvs"

# Create the virtual environment.
python3 -m venv "$HOME/venvs/dev"

# Activate the virtual environment.
# This must be done every time you open a terminal.
# You may want to add this to your .bashrc file.
source "$HOME/venvs/dev/bin/activate"
```

Install the necessary dependencies with pip.

```
pip install -r requirements.txt
```

## Usage

Run the web server.

```
./run-flask.sh
```

Access the wiki by opening a web preview browser tab on port 8080.

## License

BSD 2 Clause, see [LICENSE.txt](LICENSE.txt).
